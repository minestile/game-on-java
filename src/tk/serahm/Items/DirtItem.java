package tk.serahm.Items;

import tk.serahm.Display.GameEngine;
import tk.serahm.Display.GameObject;
import tk.serahm.Display.GameUtils;
import tk.serahm.Display.Item;
import tk.serahm.Display.Objects;

public class DirtItem extends Item {
	public DirtItem() {
		this.setTexture(GameUtils.getTextureFromPath("src/res/dirt.png"));
	}
	@Override
	public void onSelected() {
		
	}
	@Override
	public void onUse(int x, int y) {
		GameEngine.Map[y][x] = Objects.getObjectById(1);
	}
	
}
