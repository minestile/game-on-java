package tk.serahm.Items;

import tk.serahm.Display.GameEngine;
import tk.serahm.Display.GameUtils;
import tk.serahm.Display.Item;
import tk.serahm.Display.Objects;

public class StoneItem extends Item {
	public StoneItem() {
		this.setTexture(GameUtils.getTextureFromPath("src/res/stone.png"));
	}
	@Override
	public void onSelected() {
		
	}
	@Override
	public void onUse(int x, int y) {
		GameEngine.Map[y][x] = Objects.getObjectById(2);
	}
}
