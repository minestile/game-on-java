package tk.serahm.Network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.channels.Pipe;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import tk.serahm.Display.Player;

public class ConnectedClient extends Thread {
		Socket sock;
		server server;
		List<ConnectedClient> socks;
		String[] server_info;
		List<Player>  players;
		
		int id_player = -1;
		
		ConnectedClient(Socket sck, List<ConnectedClient> serv, String[] _server_info, List<Player>  _players){
			sock = sck;
			socks = serv;
			server_info = _server_info;
			players = _players;
		}
		private void toggle_client(String ip) throws IOException {
			File file;
				file = new File("clients/"+ip.replace("/", ""));
				
				if(!file.exists()) {
					file.createNewFile();
				}else {
					file.delete();
				}
		}
		public String[] sendPacket(DataOutputStream out, int id, String data) throws IOException {
			String[] res = new String[2];
			res[0] = Integer.toString(id);
			res[1] = "";
			
			System.out.println("Send packet: "+id+";"+data);
			
			String line = res[0]+";"+data;
	        
	        out.writeUTF(line);
	        out.flush();
			
			return res;
		}
		public static int randint(int Min, int Max) {
			return Min + (int)(Math.random() * ((Max - Min) + 1));
		}
		public void checkPacket(DataOutputStream out, String line) throws IOException {
			String[] request = line.split(";");
			
			if(request.length < 1) {
				this.sendPacket(out, 0, "1;Invalid request!");
			}else {
				if(Integer.parseInt(request[0]) == 1) {
					String answer = String.join(";", server_info);
					answer = answer + ";"+Integer.toString(socks.size());
					this.sendPacket(out,0,answer);
					
				}else if(Integer.parseInt(request[0]) == 2) {
					String players_pos = ""; 
					for(int i=0;i<players.size();i++) {
						players_pos = players_pos + ";" + players.get(i).id + ";" + players.get(i).x + ";" + players.get(i).y;
					}
					this.sendPacket(out,2,players.size()+""+players_pos);
					
				}else if(Integer.parseInt(request[0]) == 3) {
					if(id_player != -1) {
						this.sendPacket(out,3,"1;Player already authorized on server!");
					}else {
						int id = randint(0,9234929);
						for(int i=0;i<players.size();i++) {
							if(id != players.get(i).id)
								continue;
							else {
								id = randint(0,9234929);
								i = 0;
							}
						}
						players.add(new Player());
						players.get(players.size()-1).id = id;
						players.get(players.size()-1).x = randint(0,500);
						players.get(players.size()-1).y = randint(0,500);
						id_player = players.size()-1;
						System.out.println("Authorized on player id("+id+"), position: ("+players.get(players.size()-1).x+";"+players.get(players.size()-1).x+")");
						
						this.sendPacket(out,3,""+id);
					}
				}else if(Integer.parseInt(request[0]) == 4) {
                	for(int i=0;i<players.size();i++) {
                		if(players.get(i).id == Integer.parseInt(request[1])) {
                			players.get(i).x = Integer.parseInt(request[2]);
                			players.get(i).y = Integer.parseInt(request[3]);
                			break;
                		}
                	}
					this.sendPacket(out,4,";ok");
					
				}else{
					this.sendPacket(out,0,"2;Undefined Packet ID");
				}
			}
			
			
		}
	    public void run() {
	    	try {
		    	InputStream sin = sock.getInputStream();
		        OutputStream sout = sock.getOutputStream();
	
		        DataInputStream in = new DataInputStream(sin);
		        DataOutputStream out = new DataOutputStream(sout);
	
		        this.toggle_client(sock.getInetAddress().toString());
		        
		        String line = null;
		        while(true) {
		        	//System.out.println(socks);
		        	line = in.readUTF();
		        	System.out.println(sock.getInetAddress().toString()+" > "+line);
		        	this.checkPacket(out, line);	
		        }
	    	} catch (Exception e) {
 				// TODO Auto-generated catch block
	    		try {
					this.toggle_client(sock.getInetAddress().toString());
					this.players.remove(id_player);
					
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
 			}
	    }
}
