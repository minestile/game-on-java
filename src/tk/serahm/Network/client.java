package tk.serahm.Network;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.List;

import tk.serahm.Display.Player;

public class client extends Thread {
	List<Player> players;
	int player_id = -1;
	
	public client(List<Player> _players){
		players = _players;
	}
	public String[] sendPacket(DataInputStream in, DataOutputStream out, int id, String data) throws IOException {
		String[] res = new String[2];
		res[0] = Integer.toString(id);
		res[1] = "";
		
		String line = res[0]+";"+data;
        
        out.writeUTF(line);
        out.flush();
        
        try {
        	line = in.readUTF();
            res[1] = line;
        }catch(Exception e) {
        	System.out.println("Error => "+line);
        	res[1] = "";
        }
        
		return res;
	}
	
    public void run() {
    	int serverPort = 6666;
        //String address = "204.44.81.115";
    	String address = "127.0.0.1";

        try {
            InetAddress ipAddress = InetAddress.getByName(address); 
            System.out.println("Connecting...");
            Socket socket = new Socket(ipAddress, serverPort);
            System.out.println("OK!");

            InputStream sin = socket.getInputStream();
            OutputStream sout = socket.getOutputStream();

            DataInputStream in = new DataInputStream(sin);
            DataOutputStream out = new DataOutputStream(sout);

            
            //while (true) {
                String[] res = this.sendPacket(in, out, 1, "");
                String[] server_info = res[1].split(";");
                System.out.println("Server ip: "+server_info[1]+"\nServer port: "+server_info[2]+"\nCount players: "+(Integer.parseInt(server_info[3])-1));
                System.out.println("Register on server...");
                res = this.sendPacket(in, out, 3, "");
                player_id = Integer.parseInt(res[1].split(";")[1]);
                System.out.println("Get id player: "+player_id);
                
                while(true) {
                	//Thread.sleep(100);
                	
                	//Getting player positions
                	res = this.sendPacket(in, out, 2, "");
                	String[] players_ = res[1].split(";");
                	int players_count = Integer.parseInt(players_[1]);
                	String play_sp = "";
                	for(int i=0;i<players_.length;i++) {
                		if(i == 0 || i == 1)
                			continue;
                		play_sp = play_sp + players_[i] + ";";
                	}
                	players_ = play_sp.split(";");
                	players.clear();
                	for(int i=0;i<players_count;i++) {
                		int rel_i = (i*3);
                		
                		//System.out.println(players_[rel_i+1]+" "+players_[rel_i+2]);
                		players.add(new Player(Float.parseFloat(players_[rel_i+1]), Float.parseFloat(players_[rel_i+2]), Integer.parseInt(players_[rel_i]), 32, 32));
                	}
                	
                	//Move player
                	int local_id = -1;
                	for(int i=0;i<players.size();i++) {
                		if(players.get(i).id == player_id) {
                			local_id = i;
                			break;
                		}
                	}
                	float pl_x = players.get(local_id).x;
                	float pl_y = players.get(local_id).y;
                	
                	this.sendPacket(in, out, 4, player_id+";"+pl_x+";"+pl_y+";");
                }
            //}
        } catch (Exception x) {
            x.printStackTrace();
        }
    }
}