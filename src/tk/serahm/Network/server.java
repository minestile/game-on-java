package tk.serahm.Network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import tk.serahm.Display.Player;


public class server {
	private static List<Socket>  socks = new ArrayList<Socket>();
	private static List<ConnectedClient>  clients = new ArrayList<ConnectedClient>();
	private static List<Player>  players = new ArrayList<Player>();
	
	private String[] get_value(String key) throws IOException {
		File file = new File("vals/"+key);
		String value = "-1";
		if(file.exists()) {
			FileReader reader = new FileReader("vals/"+key);
					
			value = "";
			int c;
	        while((c=reader.read())!=-1){
	             
	        	value = value + ((char)c);
	        } 
		}
		return value.split("\n");
	}
	private static void println(String s) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println("["+dateFormat.format(date)+"] "+s);
	}
	private static void set_value(String key,String value) throws IOException {
		File file = new File("vals/"+key);
		
		if(!file.exists()) {
			file.createNewFile();
		}
		
		FileWriter fw = new FileWriter("vals/"+key, false);
		
		fw.write(value);
		fw.flush();
	}
	public static void main(String[] args) {
		System.out.println("Working Directory = " +
	              System.getProperty("user.dir"));
		
		int port = 6666;
	       try {
	    	   
	         ServerSocket ss = new ServerSocket(port); 
	         
	         final String[] server_info = new String[2];
	         server_info[0] = ss.getInetAddress().toString();
	         server_info[1] = Integer.toString(ss.getLocalPort());
	         
	         set_value("serverinfo", String.join("\n", server_info));
	         
	         System.out.println("Waiting for a clients...");
	         
	         Timer timer = new Timer();
	         timer.schedule(new TimerTask() {
	        	  @Override
	        	  public void run() {
	        		  for(int i=0;i<clients.size();i++) {
	 	        		 if(!clients.get(i).isAlive()) {
	 	        			 System.out.println("Deleted client from memory id("+i+"), ip: "+clients.get(i).sock.getInetAddress().getHostAddress());
	 	        			 try {
								clients.get(i).sock.close();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	 	        			 clients.remove(i);
	 	        		 }
	 	        	 }
	        	  }
	        	}, 100, 100);
	         
	         while(true) {
	        	 Socket socket = ss.accept(); 
	        	 socks.add(socket);
	        	 
		         System.out.println("New connection! "+socket.getInetAddress().getHostAddress()+":"+socket.getLocalPort());
		         System.out.println();
		         
		         clients.add(new ConnectedClient(socket, clients, server_info, players));
		         clients.get(clients.size()-1).start();
		         
		     
		         
		         
	         }
	 
	         
	   } catch(Exception x) { x.printStackTrace(); }
	}

}
