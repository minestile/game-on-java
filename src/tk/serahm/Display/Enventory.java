package tk.serahm.Display;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;

public class Enventory {
	public Item[] items = new Item[9];
	public int[] items_count = new int[9];
	public int selected_item = 0;
	public int dselected_item = -1;
	public int hovered_item = 0;
	
	private int ww, wh = 0;
	private int offset_h = 4;
	
	private int mouse_button = -1;
	
	public Enventory() {
		for(int i=0;i<9;i++) {
			items[i] = null;
		}
		for(int i=0;i<9;i++) {
			items_count[i] = 0;
		}
	}
	public void update(int w, int h, int mouse_x, int mouse_y, int mouse_b) {
		ww = w;
		wh = h;
		
		mouse_button = mouse_b;
		
		if(mouse_x > (ww-(9*32))/2 && mouse_x < ww-(9*32)-2*32 && mouse_y > wh+32-36 && mouse_y < wh+28) {
			int inv_x = (mouse_x-(ww-(9*32))/2)/32;
			if(inv_x < 9) {
				if(mouse_b == MouseEvent.BUTTON1)
					selected_item = inv_x;
				hovered_item = inv_x;
			}
		}else {
			hovered_item = -1;
			
			if(mouse_b == MouseEvent.BUTTON1) {
				float x = ((GameEngine.player.x/32)+((mouse_x-(GameEngine.ww/2))/32));
				float y = ((GameEngine.player.y/32)+((mouse_y-(GameEngine.wh/2))/32));
				
				items[selected_item].onUse(x,y);
				items[selected_item].onUse((int)x+1,(int)y);
				
				GameObject go = new GameObject();
				go.x = x;
				go.x = y;
				
				items[selected_item].onUse(go);
			}
			
			
		}
	}
	
	public void draw(Graphics g) {
		if(dselected_item != selected_item) {
			dselected_item = selected_item;
			if(items[selected_item] != null)
				items[selected_item].onSelected();
		}
		
		g.setColor(Color.BLACK);
		int offset_w = (ww-(9*32))/2;
		for(int i=0;i<9;i++) {
			
			if(items[i] != null) {
				g.drawImage(items[i].getTexture(),offset_w+i*32, (wh-32-offset_h), null);
				if(items_count[i] != 0) {
					g.drawString(""+items_count[i], (offset_w+i*32)+18, (wh-32-offset_h)+30);
				}
			}
			g.drawRect(offset_w+i*32, (wh-32-offset_h), 32, 32);
			
			g.setColor(Color.darkGray);
			if(hovered_item != -1)
				g.drawRect(offset_w+hovered_item*32, (wh-32-offset_h), 32, 32);
			
			g.setColor(Color.RED);
			g.drawRect(offset_w+selected_item*32, (wh-32-offset_h), 32, 32);
			
			g.setColor(Color.BLACK);
		}
		
	}
	public void set(int i, Item item, int count) {
		items[i] = item;
		items_count[i] = count;
	}
	public Item getItem(int i) {
		return items[i];
	}
	public int getCountItem(int i) {
		return items_count[i];
	}
}
