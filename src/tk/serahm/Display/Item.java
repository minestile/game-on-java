package tk.serahm.Display;

import java.awt.Image;

public class Item {
	private Image texture;
	public Image getTexture() {
		return texture;
	}
	public void setTexture(Image txt) {
		texture = txt;
	}
	public Item() {
		
	}
	public Item(Image txt) {
		texture = txt;
	}
	public void onUse(GameObject block) {
		
	}
	public void onUse(float x, float y) {
		
	}
	public void onUse(int x, int y) {
		
	}
	public void onSelected() {
		System.out.println("Selected");
	}
	public GameObject create() {
		// TODO Auto-generated method stub
		return null;
	}
}
