package tk.serahm.Display;

import java.awt.Graphics;
import java.sql.Timestamp;
import java.util.Date;

public class FpsCounter {
	int time = 0;
	public int fps = 0;
	int temp = 0;
	Player p = new Player();
	public FpsCounter() {
		update();
	}
	public void update() {
		long seconds = System.currentTimeMillis() / 1000l;
		time = (int)seconds;
	}
	public void tick(Player _p) {
		p = _p;
		temp++;
		long seconds = System.currentTimeMillis() / 1000l;
		if(time+1 <= (int)seconds) {
			fps = temp;
			temp = 0;
			time = 0;
			update();
		}
	}
	public void draw(Graphics g) {
		g.drawString("Fps: "+fps, 0, 10);
		g.drawString("(Player) x y: "+(p.x/32)+" "+(p.y/32), 0, 20);
	}
}
