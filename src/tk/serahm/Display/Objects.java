package tk.serahm.Display;

import java.util.ArrayList;
import java.util.List;

import tk.serahm.blocks.dirt;
import tk.serahm.blocks.stone;

public class Objects {
	List <GameObject> objects = new ArrayList<GameObject>();
	
	public Objects(){
		
		
	}
	
	public static GameObject getObjectById(int id) {
		if(id == 0)
			return new GameObject();
		if(id == 1)
			return new dirt();
		if(id == 2)
			return new stone();
		
		return null;
	}
}
