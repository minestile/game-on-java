package tk.serahm.Display;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import tk.serahm.Items.DirtItem;
import tk.serahm.Items.StoneItem;
import tk.serahm.blocks.dirt;
import tk.serahm.blocks.stone;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.sql.Timestamp;

public class Display {
	private static JFrame screen;
	private static Canvas canvas;
	private static int fps_count = 0;
	private static int cur_timestamp = 0;
	
	private static BufferedImage buffer;
	private static int[] bufferData;
	private static Graphics bufferGraphics;
	private static int clearColor;
	
	
	private static FpsCounter fpscount = new FpsCounter();
	
	private static int latest_key = -1;
	
	private static int[][] map;
	private static int[][] map_dump;
	private static boolean read_map = false;
	
	
	private static Enventory env = new Enventory();
	
	private static ChematicList chematics = new ChematicList();
	
	private static int ww, wh = 0;
	
	private static int mouse_button = -1;
	
	
	
	public static String getMap(String name) {
		String res = "";
		try {
			FileReader fr = new FileReader(System.getProperty("user.dir")+"/src/res/maps/"+name+".map");
			
			BufferedReader br = new BufferedReader(fr);
	
			String sCurrentLine;
			
	
			while ((sCurrentLine = br.readLine()) != null) {
				res = res + sCurrentLine+ "\n";
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}
	public static void genMap(int seed) {
		String str = "";
		for(int i=0;i<100;i++) {
			for(int j=0;j<100;j++) {
				if(i == 0) {
					str = str + 2;
					continue;
				}
				if(i == 99) {
					str = str + 2;
					continue;
				}	
				if(j == 0) {
					str = str + 2;
					continue;
				}
				if(j == 99) {
					str = str + 2;
					continue;
				}	
				
				//str = str + GameUtils.randInt(0, 2);
				str = str + 1;
			}
			str = str + "\n";
		}
		GameEngine.player.x = GameUtils.randInt(0, 99*32);
		GameEngine.player.y = GameUtils.randInt(0, 99*32);
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(System.getProperty("user.dir")+"/src/res/maps/tmp.map"));
			
		    writer.write(str);
		     
		    writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		initMap("tmp");
	}
	public static void loadMap(Map map) {
		
		
	}
	public static void initMap() {
		read_map = false;
		/*String m = "222222222222\n"
				+ "20000\n"
				+ "20000\n"
				+ "22200222222\n"
				+ "20000000002\n"
				+ "20000000002\n"
				+ "20010022222\n"
				+ "20000000002\n";*/
		
		String m = getMap("1");
		
		String[] ma = m.split("\n");
		
		
		
		int MaxSizeX = -1;
		int MaxSizeY = -1;
		for(int i=0;i<ma.length;i++) {
			if(MaxSizeX < ma[i].length())
				MaxSizeX = ma[i].length()+1;
			if(MaxSizeY < ma.length)
				MaxSizeY = ma.length+1;
		}
		
		System.out.println(MaxSizeX+" "+MaxSizeY);
		
		map = new int[MaxSizeY][MaxSizeX];
		for(int i=0;i<map.length;i++) {
			for(int j=0;j<map[i].length;j++) {
				map[i][j] = -1;
			}
		}
		GameEngine.Map = new GameObject[MaxSizeY][MaxSizeX];
		for(int i=0;i<ma.length;i++) {
			for(int j=0;j<ma[i].length();j++) {
				if((int)ma[i].charAt(j)-48 < 10) {
					map[i][j] = (int)ma[i].charAt(j)-48;
					GameEngine.Map[i][j] = Objects.getObjectById((int)ma[i].charAt(j)-48).create();
					GameEngine.Map[i][j].onInit();
				}
					
			}
		}
		
		map_dump = new int[MaxSizeY][MaxSizeX];
		for(int i=0;i<map.length;i++) {
			for(int j=0;j<map[i].length;j++) {
				map_dump[i][j] = -1;
				
			}
		}
		
		read_map = true;
	}
	public static void initMap(String name) {
		read_map = false;
		/*String m = "222222222222\n"
				+ "20000\n"
				+ "20000\n"
				+ "22200222222\n"
				+ "20000000002\n"
				+ "20000000002\n"
				+ "20010022222\n"
				+ "20000000002\n";*/
		
		String m = getMap(name);
		
		String[] ma = m.split("\n");
		
		
		
		int MaxSizeX = -1;
		int MaxSizeY = -1;
		for(int i=0;i<ma.length;i++) {
			if(MaxSizeX < ma[i].length())
				MaxSizeX = ma[i].length()+1;
			if(MaxSizeY < ma.length)
				MaxSizeY = ma.length+1;
		}
		
		System.out.println(MaxSizeX+" "+MaxSizeY);
		
		map = new int[MaxSizeY][MaxSizeX];
		for(int i=0;i<map.length;i++) {
			for(int j=0;j<map[i].length;j++) {
				map[i][j] = -1;
			}
		}
		GameEngine.Map = new GameObject[MaxSizeY][MaxSizeX];
		for(int i=0;i<ma.length;i++) {
			for(int j=0;j<ma[i].length();j++) {
				if((int)ma[i].charAt(j)-48 < 10) {
					map[i][j] = (int)ma[i].charAt(j)-48;
					GameEngine.Map[i][j] = Objects.getObjectById((int)ma[i].charAt(j)-48);
					GameEngine.Map[i][j].x = i;
					GameEngine.Map[i][j].y = j;
					if(GameEngine.Map[i][j] != null)
						GameEngine.Map[i][j].onInit();
				}
					
			}
		}
		
		map_dump = new int[MaxSizeY][MaxSizeX];
		for(int i=0;i<map.length;i++) {
			for(int j=0;j<map[i].length;j++) {
				map_dump[i][j] = -1;
				
			}
		}
		
		read_map = true;
	}
	private static boolean isCollision(int dir) {
		int x = (((int)GameEngine.player.x)/32);
		int y = (((int)GameEngine.player.y)/32);
		
		
		for(int i=x;i<x+2;i++) {
			for(int j=y;j<y+2;j++) {
				try {
					if(Objects.getObjectById(map[j][i]).rock == true) {
						if(dir == 1) //up
							GameEngine.player.y = j*32+32;
						if(dir == -1) // down
							GameEngine.player.y = j*32-33;
						if(dir == 2) // Right
							GameEngine.player.x = i*32-33;
						if(dir == -2)  //Left
							GameEngine.player.x = i*32+32;
					}
				}catch(Exception e) {
					
				}
				
			}
		}
		return false;
		
	}
	
	public static void start(int width, int height, String title, int _clearcolor) {
		ww = width;
		wh = height;
		
		GameEngine.ww = width;
		GameEngine.wh = height;
		
		
		initMap();
		
		GameEngine.player = new Player();
		
		env.set(0, new DirtItem(), 34);
		env.set(1, new StoneItem(), 12);
		
		GameEngine.player.setPosition(32, 32);
		GameEngine.player.setSize(32, 32);
		GameEngine.player.setSpeedMove(5, 5);
		
		clearColor = _clearcolor;
		
		screen = new JFrame(title);
		
		screen.setLayout(new GridLayout(0,1,0,3));
		
		
		
		
		Dimension size = new Dimension(width,height);
		canvas = new Canvas();
		canvas.setPreferredSize(size);
		
		canvas.setBackground(Color.black);
		
		screen.setResizable(false);
		screen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		screen.getContentPane().add(canvas, BorderLayout.CENTER);
		
		screen.pack();
		
		screen.setVisible(true);
		screen.setLocationRelativeTo(null);
		
		buffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		bufferData = ( (DataBufferInt) buffer.getRaster().getDataBuffer() ).getData();
		bufferGraphics = buffer.getGraphics();
		
		
		KeyEventDispatcher keyEventDispatcher = new KeyEventDispatcher() {
			  @Override
			  public boolean dispatchKeyEvent(final KeyEvent e) {
			    if (e.getID() == KeyEvent.KEY_PRESSED) {
			      int key = e.getKeyCode();
			      
			      latest_key = key;
			      
			      if(key == 38) {
			    	  //Up
			    	  GameEngine.player.moveWithSpeed(GameObject.UP);
			    	  isCollision(1);
			    	  
			      }else if(key == 37) {
			    	  //Left
			    	  GameEngine.player.moveWithSpeed(GameObject.LEFT);
			    	  isCollision(-2);
			      }else if(key == 39) {
			    	  GameEngine.player.moveWithSpeed(GameObject.RIGHT);
			    	  isCollision(2);
			      }else if(key == 40) {
			    	  //Up
			    	  GameEngine.player.moveWithSpeed(GameObject.DOWN);
			    	  isCollision(-1);
			      }else {
			    	  //GameEngine.player.moveWithSpeed(GameObject.CLEAR);
			      }
			      
			      latest_key = key;
			      
			    }else {
			    	latest_key = -1;
			    }
			    // Pass the KeyEvent to the next KeyEventDispatcher in the chain
			    return false;
			  }
			};
		
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(keyEventDispatcher);
		
		canvas.addMouseListener(new MouseListener() {
	        public void mousePressed(MouseEvent me) {
	        	mouse_button = me.getButton();
	        }
	        public void mouseReleased(MouseEvent me) {
	        	mouse_button = -1;
	        }
	        public void mouseEntered(MouseEvent me) { }
	        public void mouseExited(MouseEvent me) { }
	        public void mouseClicked(MouseEvent me) {  }
	    });
		
		genMap(1);
		
		
	}
	public static void clear() {
		Arrays.fill(bufferData, clearColor);
		
	}
	
	public static void render() throws IOException {
		
		Point p = MouseInfo.getPointerInfo().getLocation();
		Point l = screen.getLocation();
		env.update(ww, wh, p.x-l.x, p.y-l.y, mouse_button);
		
		fpscount.tick(GameEngine.player);
		
		double w = screen.getSize().getWidth();
		double h = screen.getSize().getHeight();
		

		
		
		
		if(read_map) {
			for(int i=0;i<map.length;i++) {
				for(int j=0;j<map[i].length;j++) {
					int id = map[i][j];
					int x = (int)((j*32-((int)GameEngine.player.x))+((int)(w/2)-GameEngine.player.w));
					int y = (int)((i*32-((int)GameEngine.player.y))+((int)(h/2)-GameEngine.player.h));
					
					if(x > w || y > h)
						continue;
					
					
					/*bufferGraphics.setColor(new Color(0x00ff0000));
					if(objs.getObjectById(id) != null) {
						bufferGraphics.drawImage(objs.getObjectById(id).getTexture(), x, y, null);
						objs.getObjectById(id).onDraw(x, y, bufferGraphics);
						if(map[i][j] != map_dump[i][j]) {
							objs.getObjectById(id).onInit();
						}
					}*/
					if(GameEngine.Map[i][j] != null) {
						bufferGraphics.drawImage(GameEngine.Map[i][j].getTexture(), x, y, null);
						GameEngine.Map[i][j].onDraw(x, y, bufferGraphics);
						//System.out.println(GameEngine.Map[i][j].gtt());
					}
						
					
					/*if(id==1) {
						bufferGraphics.setColor(new Color(0x00ff0000));
						//bufferGraphics.drawRect(j*32, i*32, 32, 32);
						bufferGraphics.drawImage((Image)ImageIO.read(new File("src/res/grass.png")), x, y, null);
					}
					if(id==2) {
						bufferGraphics.setColor(new Color(0x00ff0000));
						//bufferGraphics.drawRect(j*32, i*32, 32, 32);
						bufferGraphics.drawImage((Image)ImageIO.read(new File("src/res/stone.png")), x, y, null);
					}*/
				}
			}
		}
		
		map_dump = map;
		
		bufferGraphics.setColor(new Color(0xff000000));
		bufferGraphics.drawRect((int)((w/2)-GameEngine.player.w), (int)((h/2)-GameEngine.player.h), (int)GameEngine.player.w, (int)GameEngine.player.h);
		
		if(!(38 <= latest_key && latest_key <= 40) ) {
			GameEngine.player.moveWithSpeed(GameObject.CLEAR);
		}
	    	  
		//Draw fps
		fpscount.draw(bufferGraphics);

		//Draw inventory
		env.draw(bufferGraphics);
	}
	public static void swagBuffers() {
		Graphics g = canvas.getGraphics();
		g.drawImage(buffer, 0, 0, null);
	}
}
