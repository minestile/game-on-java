package tk.serahm.Display;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import javax.imageio.ImageIO;

public class GameUtils {
	public static int randInt(int min, int max) {
	    return ThreadLocalRandom.current().nextInt(min, max + 1);
	}
	public static Image getTextureFromPath(String path) {
		Image textur = null;
		try {
			textur = (Image)ImageIO.read(new File(path));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return textur;
	}
}
