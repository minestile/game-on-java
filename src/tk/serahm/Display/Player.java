package tk.serahm.Display;

import java.awt.Image;

public class Player extends GameObject {
	public Player(Image txt, boolean r) {
		super(txt, r);
		// TODO Auto-generated constructor stub
	}
	public Player() {
		super();
		// TODO Auto-generated constructor stub
		this.h = 16;
		this.w = 16;
	}

	public int id = -1;
	
	public Player(float x, float y) {
		super();
		this.x = x;
		this.y = y;
	}
	public Player(float x, float y, int id) {
		super();
		this.x = x;
		this.y = y;
		this.id = id;
	}
	public Player(float x, float y, int id, int w, int h) {
		super();
		this.x = x;
		this.y = y;
		this.id = id;
		this.w = w;
		this.h = h;
	}
}