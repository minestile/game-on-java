package tk.serahm.Display;

import java.io.BufferedReader;
import java.io.FileReader;

import tk.serahm.blocks.dirt;
import tk.serahm.blocks.stone;

public class Map {
	public static GameObject[][] map;
	public Map(){
		
	}
	private void loadFromFile(String name) {
		
		String m = "";
		try {
			FileReader fr = new FileReader(System.getProperty("user.dir")+"/src/res/maps/"+name+".map");
			
			BufferedReader br = new BufferedReader(fr);
	
			String sCurrentLine;
			
	
			while ((sCurrentLine = br.readLine()) != null) {
				m = m + sCurrentLine+ "\n";
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String[] ma = m.split("\n");
		
		
		
		int MaxSizeX = -1;
		int MaxSizeY = -1;
		for(int i=0;i<ma.length;i++) {
			if(MaxSizeX < ma[i].length())
				MaxSizeX = ma[i].length()+1;
			if(MaxSizeY < ma.length)
				MaxSizeY = ma.length+1;
		}
		
		map = new GameObject[MaxSizeX][MaxSizeY];
		for(int i=0;i<ma.length;i++) {
			for(int j=0;j<ma[i].length();j++) {
				int id = (int)ma[i].charAt(j)-48;
				if(id < 10) {
					GameObject go = new GameObject();;
					if(id == 1) {
						go = new dirt();
					}
					if(id == 2) {
						go = new stone();
					}
					map[i][j] = go;
					map[i][j].onInit();
				}
					
			}
		}
	}
}
