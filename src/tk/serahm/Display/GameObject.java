package tk.serahm.Display;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GameObject implements Cloneable {
	public float x, y,w,h = 0;
	public float speed_x,speed_y = 1;
	public float maxSpeed = (float) 1.5;
	protected Image texture;
	protected int tt = 0;
	
	
	public static int UP = 1;
	public static int DOWN = 2;
	public static int LEFT = 3;
	public static int RIGHT = 4;
	public static int CLEAR = 5;
	
	public float dx = 0;
	public float dy = 0;
	
	public boolean rock = false;
	
	public void stt(int i) {
		this.tt = i;
	}
	public int gtt() {
		return this.tt;
	}
	
	public GameObject(Image txt, boolean r) {
		rock = r;
		texture = txt;
	}
	public GameObject() {
		
	}
	public GameObject(String res, boolean r) {
		rock = r;
		try {
			texture = (Image)ImageIO.read(new File(res));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void setPosition(float _x, float _y) {
		x = _x;
		y = _y;
	}
	void setSize(float _w, float _h) {
		w = _w;
		h = _h;
	}
	void setSpeedMove(float _x, float _y) {
		speed_x = _x;
		speed_y = _y;
	}
	void moveWithSpeed(int type) {
		if(type == UP) {
			if(dy > -maxSpeed) {
				dy--;
			}
			move(0, dy);
			
		}else if(type == DOWN) {
			if(dy < maxSpeed) {
				dy++;
			}
			move(0, dy);
			
		}else if(type == LEFT) {
			if(dx > -maxSpeed) {
				dx--;
			}
			move(dx, 0);
			
		}else if(type == RIGHT) {
			if(dx < maxSpeed) {
				dx++;
			}
			move(dx, 0);
			
		}else if(type == CLEAR) {
			dy = 0;
			dx = 0;
		}
		//System.out.println(dy+" "+dx+" "+speed_x+" "+speed_y);
	}
	public void setTexture(String path) {
		try {
			this.texture = (Image)ImageIO.read(new File(path));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void move(float offset_x, float offset_y) {
		x = x + offset_x*speed_x;
		y = y + offset_y*speed_y;
	}
	
	public void onDraw(float x, float y, Graphics g) {
		
	}
	public void onInit() {
		
	}
	public Image getTexture() {
		return this.texture;
	}
	public String toString() {
		
		return "GameObject on "+this.x+", "+this.y;
	}
	public GameObject create() {

			return this;
	}
}
